package main

import (
	"fmt"

	"github.com/pkg/errors"
)

/*
 * identifiers
 */

type Identifier struct {
	name string
}

func identifier(name string) Identifier {
	return Identifier{name: name}
}

func identifiers(names ...string) []Identifier {
	result := make([]Identifier, len(names))
	for i, name := range names {
		result[i] = identifier(name)
	}
	return result
}

func enumID(i int) Identifier {
	return Identifier{name: fmt.Sprintf("%d", i)}
}

/*
 * kinds
 */

type Kind interface {
	isStar() bool
	equals(other Kind) bool
}

type HasKind interface {
	getKind() Kind
}

type Star struct{}

func (s Star) isStar() bool {
	return true
}

func (s Star) equals(other Kind) bool {
	return Kind(s) == other
}

var _ Kind = Star{}

type KFun struct {
	a, b Kind
}

func (k KFun) isStar() bool {
	return false
}

func (k KFun) equals(other Kind) bool {
	if k2, ok := other.(KFun); ok {
		return k.a.equals(k2.a) && k.b.equals(k2.b)
	}
	return false
}

var _ Kind = KFun{}

func kFun(k1, k2 Kind) KFun {
	return KFun{a: k1, b: k2}
}

func kFunN(n int) Kind {
	if n < 1 {
		return Star{}
	}
	return KFun{
		a: Star{},
		b: kFunN(n - 1),
	}
}

/*
 * types
 */

type Type interface {
	HasKind
	Types
	Instantiate

	// inHNF() returns true if the type is in "head-normal form."
	// Haskell requires class arguments to be in head-normal form,
	// meaning that they are either a type variable, or the application
	// of a type in head normal form to another type.
	inHNF() bool
}

type TVar struct {
	TYVar
}

func (t TVar) getKind() Kind {
	return t.TYVar.getKind()
}

func (t TVar) apply(sub Substitution) Types {
	return sub.replace(t.TYVar)
}

func (t TVar) tvars() TVarSet {
	return TVarSet{t.TYVar: true}
}

func (t TVar) inHNF() bool {
	return true
}

func (t TVar) inst(types []Type) Instantiate {
	return t
}

// tVarN is a helper function to create a type variable
// with a kind that requires `kn` type arguments.
func tVarN(name string, kn int) TVar {
	return TVar{
		TYVar{
			id:   identifier(name),
			kind: kFunN(kn),
		},
	}
}

var _ Type = TVar{}

type TCon struct {
	TYCon
}

func (t TCon) getKind() Kind {
	return t.TYCon.getKind()
}

func (t TCon) apply(sub Substitution) Types {
	return t
}

func (t TCon) tvars() TVarSet {
	return TVarSet{}
}

func (t TCon) inHNF() bool {
	return false
}

func (t TCon) inst(types []Type) Instantiate {
	return t
}

var _ Type = TCon{}

type TApp struct {
	a, b Type
}

func (t TApp) getKind() Kind {
	return t.a.getKind().(KFun).a
}

func (t TApp) apply(sub Substitution) Types {
	a := t.a.apply(sub).(Type)
	b := t.b.apply(sub).(Type)
	return TApp{a: a, b: b}
}

func (t TApp) tvars() TVarSet {
	return t.a.tvars().union(t.b.tvars())
}

func (t TApp) inHNF() bool {
	return t.a.inHNF()
}

func (t TApp) inst(types []Type) Instantiate {
	return TApp{
		a: t.a.inst(types).(Type),
		b: t.b.inst(types).(Type),
	}
}

var _ Type = TApp{}

type TGen struct {
	gen int
}

func (t TGen) apply(sub Substitution) Types {
	return t
}

func (t TGen) tvars() TVarSet {
	return TVarSet{}
}

func (t TGen) getKind() Kind {
	return Star{} // Right?
}

func (t TGen) inHNF() bool {
	panic("inHNF not defined for generics")
}

func (t TGen) inst(types []Type) Instantiate {
	return types[t.gen]
}

var _ Type = TGen{}

type TYVar struct {
	id   Identifier
	kind Kind
}

func (t TYVar) getKind() Kind {
	return t.kind
}

type TYCon struct {
	id   Identifier
	kind Kind
}

func (t TYCon) getKind() Kind {
	return t.kind
}

func tApp(a, b Type) TApp {
	return TApp{a: a, b: b}
}

func tConstructor(id Identifier, kind Kind) TCon {
	return TCon{
		TYCon: TYCon{
			id:   id,
			kind: kind,
		},
	}
}

// tConstructorN is a helper function for creating a TCon
// with a kind that requires `kn` type arguments.
func tConstructorN(name string, kn int) Type {
	return tConstructor(identifier(name), kFunN(kn))
}

func starType(name string) Type {
	return tConstructorN(name, 0)
}

func tUnit() Type {
	return starType("()")
}

func tInt() Type {
	return starType("Int")
}

func tChar() Type {
	return starType("Char")
}

func tFloat() Type {
	return starType("Float")
}

func tList() Type {
	return tConstructorN("[]", 1)
}

func tArrow() Type {
	return tConstructorN("(->)", 2)
}

func tTuple2() Type {
	return tConstructorN("(,)", 2)
}

func fnType(arg, ret Type) Type {
	return tApp(tApp(tArrow(), arg), ret)
}

// fnTypes assembles a function type with `ts` as the arguments
func fnTypes(arg1 Type, ts []Type) Type {
	if len(ts) == 0 {
		fmt.Println("warning: not actually constructing a function type from %v", arg1)
		return arg1
	}
	if len(ts) == 1 {
		return fnType(arg1, ts[0])
	}
	rest := ts[1:len(ts)]
	return fnType(arg1, fnTypes(ts[0], rest))
}

func pairType(a, b Type) Type {
	return tApp(tApp(tTuple2(), a), b)
}

func listType(elem Type) Type {
	return tApp(tList(), elem)
}

func tString() Type {
	return listType(tChar())
}

/*
 * substitutions
 */

type sub struct {
	tvar TYVar
	typ  Type
	next *sub
}

func (s *sub) mapF(f func(Type) Type) *sub {
	if s == nil {
		return nil
	}
	return &sub{
		tvar: s.tvar,
		typ:  f(s.typ),
		next: s.next.mapF(f),
	}
}

func (s *sub) extend(link *sub) *sub {
	if s == nil {
		return link
	}
	return &sub{
		tvar: s.tvar,
		typ:  s.typ,
		next: s.next.extend(link),
	}
}

type Substitution struct {
	subLL *sub
}

func nullSubst() Substitution {
	return Substitution{subLL: nil}
}

func singleSub(v TYVar, t Type) Substitution {
	return nullSubst().add(v, t)
}

func (s Substitution) tvars() TVarSet {
	result := TVarSet{}
	for link := s.subLL; link != nil; link = link.next {
		result[link.tvar] = true
	}
	return result
}

func (s Substitution) replace(v TYVar) Type {
	sub := s.subLL
	for sub != nil {
		if sub.tvar == v {
			return sub.typ
		}
	}
	return TVar{v}
}

func (s Substitution) add(v TYVar, t Type) Substitution {
	link := sub{tvar: v, typ: t, next: s.subLL}
	return Substitution{subLL: &link}
}

// compose combines this substitution with another,
// creating a new substitution that has the same
// effect when applied as applying `s`, then applying `sub`.
func (s Substitution) compose(sub Substitution) Substitution {
	defined := s.tvars()
	applied := s.subLL.mapF(func(t Type) Type {
		return t.apply(sub).(Type)
	})
	newSub := Substitution{subLL: applied}

	for link := sub.subLL; link != nil; link = link.next {
		if !defined[link.tvar] {
			newSub = newSub.add(link.tvar, link.typ)
		}
	}

	return newSub
}

// merge combines this substitution with another,
// creating a new substitution that has the effect of
// the union of `s` and `sub`.
// This operation fails if `s` and `sub` do something different
// for any type var.
// This is used as part of the "match" operation.
func (s Substitution) merge(sub Substitution) (*Substitution, error) {
	for tvar := range s.tvars().union(sub.tvars()) {
		result1 := TVar{tvar}.apply(s)
		result2 := TVar{tvar}.apply(sub)
		if result1 != result2 {
			return nil, errors.Errorf("substitutions disagree on %#v", tvar)
		}
	}
	return &Substitution{subLL: s.subLL.extend(sub.subLL)}, nil
}

/*
 * Types class
 */

type TVarSet map[TYVar]bool

func (t TVarSet) toList() []TYVar {
	result := make([]TYVar, 0, len(t))
	for v := range t {
		result = append(result, v)
	}
	return result
}

func (t TVarSet) union(t2 TVarSet) TVarSet {
	result := make(TVarSet, len(t)+len(t2))
	for k, v := range t {
		result[k] = v
	}
	for k, v := range t2 {
		result[k] = v
	}
	return result
}

func (t TVarSet) difference(t2 TVarSet) TVarSet {
	result := TVarSet{}
	for v := range t {
		if !t2[v] {
			result[v] = true
		}
	}
	return result
}

type Types interface {
	apply(s Substitution) Types
	tvars() TVarSet
}

/*
 * Unification
 */

func mgu(a, b Type) (*Substitution, error) {
	if tapA, ok := a.(TApp); ok {
		if tapB, ok := b.(TApp); ok {
			sub1, err := mgu(tapA.a, tapB.a)
			if err != nil {
				return nil, err
			}

			sub2, err := mgu(
				tapA.b.apply(*sub1).(Type),
				tapB.b.apply(*sub1).(Type))
			if err != nil {
				return nil, err
			}

			sub := sub1.compose(*sub2)
			return &sub, nil
		}
	}

	if tvarA, ok := a.(TVar); ok {
		return varbind(tvarA.TYVar, b)
	}

	if tvarB, ok := b.(TVar); ok {
		return varbind(tvarB.TYVar, a)
	}

	if tconA, ok := a.(TCon); ok {
		if tconB, ok := b.(TCon); ok {
			if tconA.id == tconB.id {
				sub := nullSubst()
				return &sub, nil
			}
		}
	}

	return nil, errors.Errorf("cannot unify %#v and %#v", a, b)
}

func varbind(v TYVar, t Type) (*Substitution, error) {
	if t == (TVar{v}) {
		sub := nullSubst()
		return &sub, nil
	}
	if t.tvars()[v] {
		return nil, errors.Errorf("occurs check fails, %#v is in %#v", v, t)
	}
	if v.getKind() != t.getKind() {
		return nil, errors.Errorf("kind of %#v is different from kind of %#v", v, t)
	}
	sub := singleSub(v, t)
	return &sub, nil
}

/*
 * Match
 */

func match(a, b Type) (*Substitution, error) {
	if tapA, ok := a.(TApp); ok {
		if tapB, ok := b.(TApp); ok {
			sub1, err := mgu(tapA.a, tapB.a)
			if err != nil {
				return nil, err
			}

			sub2, err := mgu(tapA.b, tapB.b)
			if err != nil {
				return nil, err
			}

			return sub1.merge(*sub2)
		}
	}

	if tvarA, ok := a.(TVar); ok {
		if tvarA.getKind() == b.getKind() {
			sub := singleSub(tvarA.TYVar, b)
			return &sub, nil
		}
	}

	if tconA, ok := a.(TCon); ok {
		if tconB, ok := b.(TCon); ok {
			if tconA.id == tconB.id {
				sub := nullSubst()
				return &sub, nil
			}
		}
	}

	return nil, errors.Errorf("cannot match %#v and %#v", a, b)
}

/*
 * 7 typeclasses
 * 7.1 basic definitions
 */

type QualType struct {
	predicates []Pred
	t          Type
}

var _ Instantiate = QualType{}

func (qt QualType) inst(types []Type) Instantiate {
	return QualType{
		predicates: instPreds(qt.predicates, types),
		t:          qt.t.inst(types).(Type),
	}
}

func (qt QualType) apply(sub Substitution) Types {
	preds := applyPreds(qt.predicates, sub)
	t := qt.t.apply(sub).(Type)
	return QualType{predicates: preds, t: t}
}

func (qt QualType) tvars() TVarSet {
	return qt.t.tvars().union(tvarsPreds(qt.predicates))
}

func (qt QualType) equals(other QualType) bool {
	if qt.t != other.t {
		return false
	}
	if len(qt.predicates) != len(other.predicates) {
		return false
	}
	for i, pred := range qt.predicates {
		if pred != other.predicates[i] {
			return false
		}
	}
	return true
}

// A predicate is a class constraint
type Pred struct {
	id  Identifier
	typ Type
}

var _ Instantiate = Pred{}

func (p Pred) inst(types []Type) Instantiate {
	return Pred{
		id:  p.id,
		typ: p.typ.inst(types).(Type),
	}
}

func (p Pred) apply(sub Substitution) Types {
	typ := p.typ.apply(sub).(Type)
	return Pred{id: p.id, typ: typ}
}

func (p Pred) tvars() TVarSet {
	return p.typ.tvars()
}

func (p Pred) overlaps(p2 Pred) bool {
	_, err := mguPred(p, p2)
	return err == nil
}

func applyPreds(predicates []Pred, sub Substitution) []Pred {
	preds := make([]Pred, len(predicates))
	for i, pred := range predicates {
		preds[i] = pred.apply(sub).(Pred)
	}
	return preds
}

func tvarsPreds(predicates []Pred) TVarSet {
	vars := TVarSet{}
	for _, pred := range predicates {
		vars = vars.union(pred.tvars())
	}
	return vars
}

func instPreds(ps []Pred, types []Type) []Pred {
	preds := make([]Pred, len(ps))
	for i, pred := range ps {
		preds[i] = pred.inst(types).(Pred)
	}
	return preds
}

func mguPred(a, b Pred) (*Substitution, error) {
	if a.id == b.id {
		return mgu(a.typ, b.typ)
	}
	return nil, errors.Errorf("classes differ in %#v and %#v", a, b)
}

func matchPred(a, b Pred) (*Substitution, error) {
	if a.id == b.id {
		return match(a.typ, b.typ)
	}
	return nil, errors.Errorf("classes differ in %#v and %#v", a, b)
}

type Class struct {
	ids   []Identifier // superclasses
	insts []Inst       // instances of the class
}

// effectively, a `Qual Pred`
type Inst struct {
	predicates []Pred
	p          Pred
}

var _ Instantiate = Inst{}

func (ins Inst) inst(types []Type) Instantiate {
	return Inst{
		predicates: instPreds(ins.predicates, types),
		p:          ins.p.inst(types).(Pred),
	}
}

func (ins Inst) apply(sub Substitution) Types {
	preds := applyPreds(ins.predicates, sub)
	p := ins.p.apply(sub).(Pred)
	return Inst{predicates: preds, p: p}
}

func (ins Inst) tvars() TVarSet {
	return ins.p.tvars().union(tvarsPreds(ins.predicates))
}

func pred(name string, cls Type) Pred {
	return Pred{
		id:  identifier(name),
		typ: cls,
	}
}

// example
func tOrd() Class {
	return Class{
		ids: []Identifier{identifier("Eq")},
		insts: []Inst{
			{p: pred("Ord", tUnit())},
			{p: pred("Ord", tChar())},
			{p: pred("Ord", tInt())},
			{
				predicates: []Pred{
					pred("Ord", tVarN("a", 0)),
					pred("Ord", tVarN("b", 0)),
				},
				p: pred("Ord", pairType(tVarN("a", 0), tVarN("b", 0))),
			},
		},
	}
}

/*
 * 7.2 class environments
 */

type ClassEnv struct {
	classes  map[Identifier]*Class
	defaults []Type
}

func (ce *ClassEnv) isDefined(id Identifier) bool {
	_, ok := ce.classes[id]
	return ok
}

func (ce *ClassEnv) class(id Identifier) (*Class, error) {
	if class, ok := ce.classes[id]; ok {
		return class, nil
	}
	return nil, errors.Errorf("class %s not defined", id.name)
}

func (ce *ClassEnv) super(id Identifier) ([]Identifier, error) {
	class, err := ce.class(id)
	if err != nil {
		return nil, err
	}
	return class.ids, nil
}

func (ce *ClassEnv) insts(id Identifier) ([]Inst, error) {
	class, err := ce.class(id)
	if err != nil {
		return nil, err
	}
	return class.insts, nil
}

func (ce ClassEnv) modify(id Identifier, class Class) ClassEnv {
	classes := map[Identifier]*Class{
		id: &class,
	}
	for i, c := range ce.classes {
		classes[i] = c
	}
	return ClassEnv{classes: classes, defaults: ce.defaults}
}

func initialEnv() ClassEnv {
	return ClassEnv{
		classes:  make(map[Identifier]*Class),
		defaults: []Type{tInt(), tFloat()},
	}
}

type EnvTransformer func(*ClassEnv) (*ClassEnv, error)

func composeTxns(txns ...EnvTransformer) EnvTransformer {
	return func(ce *ClassEnv) (*ClassEnv, error) {
		var err error
		for _, tx := range txns {
			ce, err = tx(ce)
			if err != nil {
				return nil, err
			}
		}
		return ce, nil
	}
}

func addClass(className string, superClasses []string) EnvTransformer {
	id := identifier(className)

	ids := make([]Identifier, len(superClasses))
	for i, super := range superClasses {
		ids[i] = identifier(super)
	}

	return func(ce *ClassEnv) (*ClassEnv, error) {
		if ce.isDefined(id) {
			return nil, errors.Errorf("class %s is already defined", id.name)
		}
		for _, super := range ids {
			if !ce.isDefined(super) {
				return nil, errors.Errorf("super class %s is not defined", super.name)
			}
		}

		class := Class{
			ids:   ids,
			insts: []Inst{},
		}
		ce2 := ce.modify(id, class)
		return &ce2, nil
	}
}

func addInst(ps []Pred, p Pred) EnvTransformer {
	return func(ce *ClassEnv) (*ClassEnv, error) {
		its, err := ce.insts(p.id)
		if err != nil {
			return nil, err
		}

		qs := make([]Pred, len(its))
		for i, inst := range its {
			if p.overlaps(inst.p) {
				return nil, errors.Errorf("overlapping instance")
			}
			qs[i] = inst.p
		}

		supers, _ := ce.super(p.id)
		class := Class{
			ids:   supers,
			insts: append([]Inst{{predicates: ps, p: p}}, its...),
		}
		ce2 := ce.modify(p.id, class)
		return &ce2, nil
	}
}

var addPreludeClasses = composeTxns(addCoreClasses, addNumClasses)

var addCoreClasses = composeTxns(
	addClass("Eq", nil),
	addClass("Ord", []string{"Eq"}),
	addClass("Show", nil),
	addClass("Read", nil),
	addClass("Bounded", nil),
	addClass("Enum", nil),
	addClass("Functor", nil),
	addClass("Monad", nil),
)

var addNumClasses = composeTxns(
	addClass("Num", []string{"Eq", "Show"}),
	addClass("Real", []string{"Num", "Ord"}),
	addClass("Fractional", []string{"Num"}),
	addClass("Integral", []string{"Real", "Enum"}),
	addClass("RealFrac", []string{"Real", "Fractional"}),
	addClass("Floating", []string{"Fractional"}),
	addClass("RealFloat", []string{"RealFrac", "Floating"}),
)

var exampleInsts = composeTxns(
	addPreludeClasses,
	addInst([]Pred{}, pred("Ord", tUnit())),
	addInst([]Pred{}, pred("Ord", tChar())),
	addInst([]Pred{}, pred("Ord", tInt())),
	addInst(
		[]Pred{
			pred("Ord", tVarN("a", 0)),
			pred("Ord", tVarN("b", 0)),
		},
		pred("Ord", pairType(tVarN("a", 0), tVarN("b", 0))),
	),
)

func preludeEnv() (*ClassEnv, error) {
	initial := initialEnv()
	return exampleInsts(&initial)
}

/*
 * 7.3 Enailment
 */

func (ce *ClassEnv) bySuper(p Pred) []Pred {
	result := []Pred{p}
	supers, err := ce.super(p.id)
	if err != nil {
		panic(err)
	}

	for _, super := range supers {
		preds := ce.bySuper(pred(super.name, p.typ))
		result = append(result, preds...)
	}

	return result
}

func (ce *ClassEnv) byInst(p Pred) []Pred {
	its, err := ce.insts(p.id)
	if err != nil {
		panic(err)
	}

	for _, it := range its {
		sub, err := matchPred(it.p, p)
		if err != nil {
			continue
		}

		return applyPreds(it.predicates, *sub)
	}
	return nil // nothing matched
}

func (ce *ClassEnv) entails(ps []Pred, p Pred) bool {
	for _, pred := range ps {
		for _, element := range ce.bySuper(pred) {
			if p == element {
				return true
			}
		}
	}

	insts := ce.byInst(p)
	if insts == nil {
		return false
	}

	for _, q := range insts {
		if !ce.entails(ps, q) {
			return false
		}
	}
	return true
}

/*
 * 7.4 context reduction
 */

func (p Pred) inHNF() bool {
	return p.typ.inHNF()
}

func (ce *ClassEnv) toHNFs(ps []Pred) ([]Pred, error) {
	var results []Pred
	for _, pred := range ps {
		pss, err := ce.toHNF(pred)
		if err != nil {
			return nil, err
		}
		results = append(results, pss...)
	}
	return results, nil
}

func (ce *ClassEnv) toHNF(p Pred) ([]Pred, error) {
	if p.inHNF() {
		return []Pred{p}, nil
	}

	// TODO: This might be wrong. byInst may panic when it should return nil.
	ps := ce.byInst(p)
	if ps == nil {
		return nil, errors.Errorf("context reduction fails")
	}
	return ce.toHNFs(ps)
}

func (ce *ClassEnv) simplify(ps []Pred) []Pred {
	var results []Pred
	for i, pred := range ps {
		var current []Pred
		current = append(current, results...)
		current = append(current, ps[i+1:]...)
		if !ce.entails(current, pred) {
			results = append(results, pred)
		}
	}
	return results
}

func (ce *ClassEnv) reduce(ps []Pred) ([]Pred, error) {
	qs, err := ce.toHNFs(ps)
	if err != nil {
		return nil, err
	}
	return ce.simplify(qs), nil
}

func (ce *ClassEnv) scEntail(ps []Pred, p Pred) bool {
	for _, pred := range ps {
		for _, elem := range ce.bySuper(pred) {
			if elem == p {
				return true
			}
		}
	}
	return false
}

/*
 * 8 Type Schemes
 */

type Scheme struct {
	kinds []Kind
	qt    QualType
}

var _ Types = Scheme{}

func (s Scheme) apply(sub Substitution) Types {
	return Scheme{
		kinds: s.kinds,
		qt:    s.qt.apply(sub).(QualType),
	}
}

func (s Scheme) tvars() TVarSet {
	return s.qt.tvars()
}

func (s Scheme) equals(other Scheme) bool {
	if !s.qt.equals(other.qt) {
		return false
	}
	if len(s.kinds) != len(other.kinds) {
		return false
	}
	for i, k := range s.kinds {
		if !k.equals(other.kinds[i]) {
			return false
		}
	}
	return true
}

func quantify(vars []TYVar, qt QualType) Scheme {
	ftvs := qt.tvars()

	var ks []Kind
	var sub Substitution

	i := 0
	for _, v := range vars {
		if ftvs[v] {
			ks = append(ks, v.kind)
			sub = sub.add(v, TGen{i})
			i++
		}
	}

	return Scheme{
		kinds: ks,
		qt:    qt.apply(sub).(QualType),
	}
}

func toScheme(t Type) Scheme {
	return Scheme{qt: QualType{t: t}}
}

/*
 * Assumptions
 */

type Assump struct {
	id  Identifier
	sch Scheme
}

var _ Types = Assump{}

func (a Assump) apply(sub Substitution) Types {
	return Assump{
		id:  a.id,
		sch: a.sch.apply(sub).(Scheme),
	}
}

func (a Assump) tvars() TVarSet {
	return a.sch.tvars()
}

func find(id Identifier, as []Assump) (*Scheme, error) {
	for _, assump := range as {
		if assump.id == id {
			scheme := assump.sch
			return &scheme, nil
		}
	}
	return nil, errors.Errorf("unbound identifier: %s", id.name)
}

/*
 * 10 Type Inference structure
 */

type Infer struct {
	varn int
	sub  Substitution
}

func (i *Infer) unify(a, b Type) error {
	sub, err := mgu(a.apply(i.sub).(Type), b.apply(i.sub).(Type))
	if err != nil {
		return err
	}
	i.extendSub(*sub)
	return nil
}

func (i *Infer) extendSub(sub Substitution) {
	i.sub = i.sub.compose(sub)
}

func (i *Infer) newTVar(kind Kind) Type {
	n := i.varn
	i.varn++
	return TVar{TYVar{id: enumID(n), kind: kind}}
}

func (sc Scheme) freshInstance(infer *Infer) QualType {
	ts := make([]Type, len(sc.kinds))
	for i, kind := range sc.kinds {
		ts[i] = infer.newTVar(kind)
	}

	return sc.qt.inst(ts).(QualType)
}

type Instantiate interface {
	inst(types []Type) Instantiate
}

/*
 * 11 Type Inference
 * 11.1 Literals
 */

type Literal interface {
	tiLit(infer *Infer) ([]Pred, Type)
}

type LitInt int64

var _ Literal = LitInt(0)

func (_ LitInt) tiLit(infer *Infer) ([]Pred, Type) {
	v := infer.newTVar(Star{})
	return []Pred{pred("Num", v)}, v
}

type LitChar rune

var _ Literal = LitChar(' ')

func (_ LitChar) tiLit(infer *Infer) ([]Pred, Type) {
	return []Pred{}, tChar()
}

type LitFloat float64

var _ Literal = LitFloat(0)

func (_ LitFloat) tiLit(infer *Infer) ([]Pred, Type) {
	v := infer.newTVar(Star{})
	return []Pred{pred("Fractional", v)}, v
}

type LitStr string

var _ Literal = LitStr("")

func (_ LitStr) tiLit(infer *Infer) ([]Pred, Type) {
	return []Pred{}, tString()
}

/*
 * 11.2 Patterns
 */

type Pattern interface {
	tiPat(infer *Infer) ([]Pred, []Assump, Type, error)
}

type PVar struct {
	id Identifier
}

var _ Pattern = PVar{}

func (pv PVar) tiPat(infer *Infer) ([]Pred, []Assump, Type, error) {
	v := infer.newTVar(Star{})
	asp := Assump{id: pv.id, sch: toScheme(v)}
	return []Pred{}, []Assump{asp}, v, nil
}

type PWildcard struct{}

var _ Pattern = PWildcard{}

func (pw PWildcard) tiPat(infer *Infer) ([]Pred, []Assump, Type, error) {
	v := infer.newTVar(Star{})
	return []Pred{}, []Assump{}, v, nil
}

type PAs struct {
	id  Identifier
	pat Pattern
}

var _ Pattern = PAs{}

func (pa PAs) tiPat(infer *Infer) ([]Pred, []Assump, Type, error) {
	ps, as, t, err := pa.pat.tiPat(infer)
	if err != nil {
		return nil, nil, nil, err
	}
	assump := Assump{id: pa.id, sch: toScheme(t)}
	assumps := append([]Assump{assump}, as...)
	return ps, assumps, t, nil
}

type PLit struct {
	lit Literal
}

var _ Pattern = PLit{}

func (pl PLit) tiPat(infer *Infer) ([]Pred, []Assump, Type, error) {
	ps, t := pl.lit.tiLit(infer)
	return ps, []Assump{}, t, nil
}

type PNpk struct {
	id Identifier
	n  int
}

var _ Pattern = PNpk{}

func (pn PNpk) tiPat(infer *Infer) ([]Pred, []Assump, Type, error) {
	t := infer.newTVar(Star{})
	p := pred("Integral", t)
	a := Assump{id: pn.id, sch: toScheme(t)}
	return []Pred{p}, []Assump{a}, t, nil
}

type PCon struct {
	assump Assump
	ps     []Pattern
}

var _ Pattern = PCon{}

func (pc PCon) tiPat(infer *Infer) ([]Pred, []Assump, Type, error) {
	ps, as, ts, err := tiPats(pc.ps, infer)
	if err != nil {
		return nil, nil, nil, err
	}

	t2 := infer.newTVar(Star{})
	qt := pc.assump.sch.freshInstance(infer)
	fnT := fnTypes(t2, ts)
	if err := infer.unify(qt.t, fnT); err != nil {
		return nil, nil, nil, err
	}

	resultPs := append(ps, qt.predicates...)
	return resultPs, as, t2, nil
}

func tiPats(pats []Pattern, infer *Infer) (ps []Pred, as []Assump, ts []Type, err error) {
	for _, pattern := range pats {
		preds, assumps, t, err := pattern.tiPat(infer)
		if err != nil {
			return nil, nil, nil, err
		}
		ps = append(ps, preds...)
		as = append(as, assumps...)
		ts = append(ts, t)
	}

	return
}

/*
 * 11.3 Expressions
 */

type Expression interface {
	tiExpr(infer *Infer, ce ClassEnv, assumps []Assump) ([]Pred, Type, error)
}

type Var struct {
	id Identifier
}

var _ Expression = Var{}

func (v Var) tiExpr(infer *Infer, ce ClassEnv, assumps []Assump) ([]Pred, Type, error) {
	sc, err := find(v.id, assumps)
	if err != nil {
		return nil, nil, err
	}
	qt := sc.freshInstance(infer)
	return qt.predicates, qt.t, nil
}

type Lit struct {
	lit Literal
}

var _ Expression = Lit{}

func (l Lit) tiExpr(infer *Infer, ce ClassEnv, assumps []Assump) ([]Pred, Type, error) {
	ps, t := l.lit.tiLit(infer)
	return ps, t, nil
}

type Const struct {
	assump Assump
}

var _ Expression = Const{}

func (c Const) tiExpr(infer *Infer, ce ClassEnv, assumps []Assump) ([]Pred, Type, error) {
	qt := c.assump.sch.freshInstance(infer)
	return qt.predicates, qt.t, nil
}

type Ap struct {
	fn, arg Expression
}

var _ Expression = Ap{}

func (a Ap) tiExpr(infer *Infer, ce ClassEnv, assumps []Assump) ([]Pred, Type, error) {
	ps, tFn, err := a.fn.tiExpr(infer, ce, assumps)
	if err != nil {
		return nil, nil, err
	}
	qs, tArg, err := a.arg.tiExpr(infer, ce, assumps)
	if err != nil {
		return nil, nil, err
	}

	t := infer.newTVar(Star{})
	if err := infer.unify(fnType(tArg, t), tFn); err != nil {
		return nil, nil, err
	}

	return append(ps, qs...), t, nil
}

type Let struct {
	bindings BindGroup
	body     Expression
}

var _ Expression = Let{}

func (l Let) tiExpr(infer *Infer, ce ClassEnv, assumps []Assump) ([]Pred, Type, error) {
	ps, as, err := l.bindings.tiBindGroup(infer, ce, assumps)
	if err != nil {
		return nil, nil, err
	}
	qs, t, err := l.body.tiExpr(infer, ce, append(as, assumps...))
	if err != nil {
		return nil, nil, err
	}
	return append(ps, qs...), t, nil
}

// TODO: Add lambda abstractions to my version

/*
 * 11.4 Alternatives
 */

type Alt struct {
	pats []Pattern
	expr Expression
}

func (a Alt) tiAlt(infer *Infer, ce ClassEnv, as []Assump) ([]Pred, Type, error) {
	ps, assumps, ts, err := tiPats(a.pats, infer)
	if err != nil {
		return nil, nil, err
	}

	qs, t, err := a.expr.tiExpr(infer, ce, append(assumps, as...))
	if err != nil {
		return nil, nil, err
	}

	return append(ps, qs...), fnTypes(t, ts), nil
}

func tiAlts(infer *Infer, ce ClassEnv, as []Assump, alts []Alt, t Type) ([]Pred, error) {
	var ps []Pred
	var ts []Type
	for _, alt := range alts {
		preds, t, err := alt.tiAlt(infer, ce, as)
		if err != nil {
			return nil, err
		}
		ps = append(ps, preds...)
		ts = append(ts, t)
	}

	for _, typ := range ts {
		err := infer.unify(t, typ)
		if err != nil {
			return nil, err
		}
	}

	return ps, nil
}

/*
 * 11.5 From Types to Type Schemes
 */

func split(
	ce ClassEnv, fs []TYVar, gs []TYVar, ps []Pred,
) (deferred []Pred, retained []Pred, err error) {
	ps2, err := ce.reduce(ps)
	if err != nil {
		return nil, nil, err
	}

	fsSet := map[TYVar]bool{}
	for _, v := range fs {
		fsSet[v] = true
	}

	// partition predicates into deferred predicates (ds)
	// whcich contain only
	var ds []Pred
	var rs []Pred
	for _, pred := range ps2 {
		allInFS := true

		for v := range pred.tvars() {
			allInFS = allInFS && fsSet[v]
		}

		if allInFS {
			ds = append(ds, pred)
		} else {
			rs = append(rs, pred)
		}
	}

	fsgs := append(append([]TYVar{}, fs...), gs...)
	defaulted, err := defaultedPreds(ce, fsgs, rs)
	if err != nil {
		return nil, nil, err
	}

	retained = []Pred{}
	for _, pred := range rs {
		isDefaulted := false
		for _, d := range defaulted {
			if d == pred {
				isDefaulted = true
				break
			}
		}
		if !isDefaulted {
			retained = append(retained, pred)
		}
	}

	return ds, retained, nil
}

type Ambiguity struct {
	tv    TYVar
	preds []Pred
}

func ambiguities(vs []TYVar, ps []Pred) []Ambiguity {
	vsSet := map[TYVar]bool{}
	for _, v := range vs {
		vsSet[v] = true
	}

	result := []Ambiguity{}
	for v := range tvarsPreds(ps) {
		if !vsSet[v] {
			preds := []Pred{}
			for _, pred := range ps {
				if pred.tvars()[v] {
					preds = append(preds, pred)
				}
			}

			result = append(result, Ambiguity{
				tv:    v,
				preds: preds,
			})
		}
	}
	return result
}

var numClasses = identifiers(
	"Num", "Integral", "Floating", "Fractional",
	"Real", "RealFloat", "RealFrac",
)

var stdClasses = append(
	identifiers(
		"Eq", "Ord", "Show", "Read", "Bounded", "Enum",
		"Ix", "Functor", "Monad", "MonadPlus",
	),
	numClasses...)

// candidates finds types that might resolve an ambiguity
func candidates(ce ClassEnv, am Ambiguity) []Type {
	v := am.tv
	qs := am.preds

	// all ((TVar v) ==) ts
	for _, pred := range qs {
		if pred.typ != (TVar{v}) {
			return nil
		}
	}

	// any (`elem` numClasses) is
	if !hasNumClass(qs) {
		return nil
	}

	// all (`elem` stdClasses) is
	if !allStdClasses(qs) {
		return nil
	}

	var types []Type
	for _, t := range ce.defaults {
		// all (entail ce []) [IsIn i t' | i <- is]
		allEntailed := allPreds(qs, func(p Pred) bool {
			p2 := Pred{id: p.id, typ: t}
			return ce.entails([]Pred{}, p2)
		})
		if allEntailed {
			types = append(types, t)
		}
	}

	return types
}

func allStdClasses(ps []Pred) bool {
	return allPreds(ps, isStdClass)
}

func isStdClass(p Pred) bool {
	return idIn(p.id, stdClasses)
}

func hasNumClass(ps []Pred) bool {
	return anyPreds(ps, isNumClass)
}

func isNumClass(p Pred) bool {
	return idIn(p.id, numClasses)
}

func allPreds(ps []Pred, fn func(Pred) bool) bool {
	for _, pred := range ps {
		if !fn(pred) {
			return false
		}
	}
	return true
}

func anyPreds(ps []Pred, fn func(Pred) bool) bool {
	for _, pred := range ps {
		if fn(pred) {
			return true
		}
	}
	return false
}

func idIn(id Identifier, ids []Identifier) bool {
	for _, i := range ids {
		if id == i {
			return true
		}
	}
	return false
}

func withDefaults(
	fn func([]Ambiguity, []Type) interface{},
	ce ClassEnv,
	vs []TYVar,
	ps []Pred,
) (interface{}, error) {
	vps := ambiguities(vs, ps)
	var types []Type
	for _, am := range vps {
		ts := candidates(ce, am)
		if len(ts) == 0 {
			return nil, errors.Errorf("cannot resolve ambiguity %#v", am)
		}
		types = append(types, ts[0])
	}

	return fn(vps, types), nil
}

func defaultedPreds(ce ClassEnv, vs []TYVar, ps []Pred) ([]Pred, error) {
	fn := func(vps []Ambiguity, ts []Type) interface{} {
		var ps []Pred
		for _, am := range vps {
			ps = append(ps, am.preds...)
		}
		return ps
	}
	results, err := withDefaults(fn, ce, vs, ps)
	if err != nil {
		return nil, err
	}
	return results.([]Pred), nil
}

func defaultSubst(ce ClassEnv, vs []TYVar, ps []Pred) (Substitution, error) {
	fn := func(vps []Ambiguity, ts []Type) interface{} {
		var sub Substitution
		for i, am := range vps {
			sub = sub.add(am.tv, ts[i])
		}
		return sub
	}
	results, err := withDefaults(fn, ce, vs, ps)
	if err != nil {
		return Substitution{}, err
	}
	return results.(Substitution), nil
}

/*
 * 11.6 Binding Groups
 */

// Expl is an explicitly-typed binding
type Expl struct {
	id   Identifier
	sch  Scheme
	alts []Alt
}

func (e *Expl) tiExpl(infer *Infer, ce ClassEnv, as []Assump) ([]Pred, error) {
	qual := e.sch.freshInstance(infer)
	ps, err := tiAlts(infer, ce, as, e.alts, qual.t)
	if err != nil {
		return nil, err
	}

	sub := infer.sub
	qs := applyPreds(qual.predicates, sub)
	t := qual.t.apply(sub).(Type)
	fs := tvarsAssumps(applyAssumps(as, sub))
	gs := t.tvars().difference(fs)
	sc := quantify(gs.toList(), QualType{predicates: qs, t: t})

	var ps2 []Pred
	for _, p := range applyPreds(ps, sub) {
		if !ce.entails(qs, p) {
			ps2 = append(ps2, p)
		}
	}

	ds, rs, err := split(ce, fs.toList(), gs.toList(), ps2)
	if err != nil {
		return nil, err
	}

	if !e.sch.equals(sc) {
		return nil, errors.Errorf(
			"signature of %s is too general", e.id.name)
	}

	if len(rs) > 0 {
		return nil, errors.Errorf(
			"context for %s is too weak", e.id.name)
	}

	return ds, nil
}

// Impl is an implicitly-typed binding
type Impl struct {
	id   Identifier
	alts []Alt
}

// restricted tests if the monomorphism restriction applies
func restricted(impls []Impl) bool {
	for _, impl := range impls {
		for _, alt := range impl.alts {
			if len(alt.pats) == 0 {
				return true
			}
		}
	}
	return false
}

func tiImpls(
	infer *Infer, ce ClassEnv, impls []Impl, as []Assump,
) ([]Pred, []Assump, error) {
	var assumps []Assump
	assumps = append(assumps, as...)

	ts := make([]Type, len(impls))
	is := make([]Identifier, len(impls))
	scs := make([]Scheme, len(impls))
	altss := make([][]Alt, len(impls))

	// Create assumptions for each binding
	for i, impl := range impls {
		t := infer.newTVar(Star{})
		sch := toScheme(t)
		ts[i] = t
		is[i] = impl.id
		scs[i] = sch
		altss[i] = impl.alts
		assumps = append(assumps, Assump{id: impl.id, sch: sch})
	}

	// Infer the types for the bindings
	var preds []Pred
	for i, alts := range altss {
		ps, err := tiAlts(infer, ce, assumps, alts, ts[i])
		if err != nil {
			return nil, nil, err
		}
		preds = append(preds, ps...)
	}

	preds2 := applyPreds(preds, infer.sub)
	_ = preds2
	ts2 := make([]Type, len(ts))
	for i, t := range ts {
		ts2[i] = t.apply(infer.sub).(Type)
	}

	fs := tvarsAssumps(as)

	vs := TVarSet{}
	for _, t := range ts2 {
		vs = vs.union(t.tvars())
	}

	gs := vs.difference(fs)
	gsList := gs.toList()

	ds, rs, err := split(ce, fs.toList(), vs.toList(), preds2)
	if err != nil {
		return nil, nil, err
	}

	if restricted(impls) {
		rsTV := tvarsPreds(rs)
		gs2 := gs.difference(rsTV)

		scs2 := make([]Scheme, len(impls))
		for i, t := range ts2 {
			scs2[i] = quantify(gs2.toList(), QualType{t: t})
		}

		var ps []Pred
		ps = append(ps, ds...)
		ps = append(ps, rs...)

		assumps2 := make([]Assump, len(impls))
		for i, impl := range impls {
			assumps2[i] = Assump{id: impl.id, sch: scs2[i]}
		}

		return ps, assumps2, nil
	}

	scs2 := make([]Scheme, len(impls))
	for i, t := range ts2 {
		scs2[i] = quantify(gsList, QualType{predicates: rs, t: t})
	}

	assumps2 := make([]Assump, len(impls))
	for i, impl := range impls {
		assumps2[i] = Assump{id: impl.id, sch: scs2[i]}
	}

	return ds, assumps2, nil
}

func applyAssumps(as []Assump, sub Substitution) []Assump {
	result := make([]Assump, len(as))
	for i, a := range as {
		result[i] = a.apply(sub).(Assump)
	}
	return result
}

func tvarsAssumps(as []Assump) TVarSet {
	result := TVarSet{}
	for _, a := range as {
		result = result.union(a.tvars())
	}
	return result
}

type BindGroup struct {
	expls []Expl
	impls [][]Impl
}

func (bg BindGroup) tiBindGroup(
	infer *Infer, ce ClassEnv, as []Assump,
) ([]Pred, []Assump, error) {
	var assumps []Assump
	var preds []Pred

	assumps = append(assumps, as...)

	// Start by assuming that expllicity-typed things have the type that they
	// say they do
	for _, expl := range bg.expls {
		assumps = append(assumps, Assump{id: expl.id, sch: expl.sch})
	}

	// Infer the type of implicitly-typed bindings
	for _, impls := range bg.impls {
		ps, as, err := tiImpls(infer, ce, impls, assumps)
		if err != nil {
			return nil, nil, err
		}

		preds = append(preds, ps...)
		assumps = append(assumps, as...)
	}

	// Go back and check that the types given for the explicitly-typed bindings
	// work.
	for _, expl := range bg.expls {
		ps, err := expl.tiExpl(infer, ce, assumps)
		if err != nil {
			return nil, nil, err
		}

		preds = append(preds, ps...)
	}

	return preds, assumps, nil
}

type Program struct {
	bgs []BindGroup
}

func (p Program) tiProgram(ce ClassEnv, as []Assump) ([]Assump, error) {
	infer := &Infer{}

	var preds []Pred
	assumps := append([]Assump{}, as...)
	for _, bindGroup := range p.bgs {
		ps, as2, err := bindGroup.tiBindGroup(infer, ce, assumps)
		if err != nil {
			return nil, err
		}

		preds = append(preds, ps...)
		assumps = append(assumps, as2...)
	}

	s := infer.sub

	subbedPs := make([]Pred, len(preds))
	for i, pred := range preds {
		subbedPs[i] = pred.apply(s).(Pred)
	}

	rs, err := ce.reduce(subbedPs)
	if err != nil {
		return nil, err
	}

	s2, err := defaultSubst(ce, []TYVar{}, rs)

	sub := s.compose(s2)
	return applyAssumps(assumps, sub), nil
}

func main() {
	t := fnTypes(tInt(), []Type{tInt(), tChar()})
	fmt.Println(t)
}
